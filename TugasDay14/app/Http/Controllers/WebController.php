<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function getWelcome () {
        return view('welcome');
    }

    public function getTable () {
        return view('pages.table');
    }

    public function getdataTable () {
        return view('pages.datatable');
    }

    public function getContact() {
        return view('pages.contact');
    }
}
