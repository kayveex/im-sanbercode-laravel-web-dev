<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SanberBook | Homepage</title>
  </head>
  <body>
    <h1 id="title">SanberBook</h1>
    <div class="description">
      <h2 id="article-title">Social Media Developer Santai Berkualitas</h2>
      <p class="paragraph">Belajar dan Berbagi agar hidup ini semakin santai berkualitas.</p>
    </div>
    <div class="content-section">
      <h3 class="heading-3">Benefit Join di SanberBook</h3>
      <ul>
        <li>Mendapatkan motivasi dari sesama developer.</li>
        <li>Sharing knowledge dari para mastah Sanber.</li>
        <li>Dibuat oleh calon web developer terbaik.</li>
      </ul>
    </div>
    <div class="content-section">
      <h3 class="heading-3">Cara Bergabung ke SanberBook</h3>
      <ol>
        <li>Mengunjungi Website ini.</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
      </ol>
    </div>
  </body>
</html>
