<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sanberbook | Make A New Account</title>
  </head>
  <body>
    <h1 class="title">Buat Account Baru!</h1>
    <h2 class="heading-2">Sign Up Form</h2>
    <form action="/welcome" method="post"> 
        {{-- <form action="/nama_route_dari_web.php" method="post">  --}}
      <section role="region" aria-label="user-data">
        @csrf
        <div class="info">
          <label for="first-name">First Name: </label>
          <br />
          <br />
          <input type="text" name="first-name" id="first-name" />
        </div>
        <div class="info">
          <br />
          <label for="last-name">Last Name: </label>
          <br />
          <br />
          <input type="text" name="last-name" id="last-name" />
        </div>
        <div class="info">
          <br />
          <label for="gender">Gender: </label>
          <br /><br />
          <input type="radio" name="gender" id="Male" />Male
          <br />
          <input type="radio" name="gender" id="Female" />Female
          <br />
          <input type="radio" name="gender" id="Other" />Other
        </div>
        <div class="info">
          <br />
          <label for="Nationality">Nationality: </label>
          <br />
          <br />
          <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="sea">South East Asia</option>
            <option value="asia">Asia</option>
            <option value="america">America</option>
            <option value="europe">Europe</option>
            <option value="africa">Africa</option>
          </select>
        </div>
        <div class="info">
          <br />
          <label for="language-spoken">Language Spoken: </label>
          <br />
          <br />
          <input type="checkbox" name="language-spoken" /> Bahasa Indonesia
          <br />
          <input type="checkbox" name="language-spoken" /> English
          <br />
          <input type="checkbox" name="language-spoken" /> Other
          <br />
        </div>
        <div class="info">
          <br />
          <label for="bio">Bio: </label>
          <br />
          <br />
          <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
          <br />
          <br />
          <input type="submit" value="Submit" />
        </div>
      </section>
    </form>
  </body>
</html>
