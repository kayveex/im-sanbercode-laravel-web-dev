<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function sendForm(Request $request) {
        $namaDepan = $request['first-name'];
        $namaBelakang = $request['last-name'];
        return view('halaman.welcome',['namaDepan'=> $namaDepan,'namaBelakang'=>$namaBelakang]);
    }

    public function register() {
        return view('halaman.register');
    }
}
