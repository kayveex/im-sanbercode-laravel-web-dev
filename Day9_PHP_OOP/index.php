<?php
    require('animal.php');
    require ('Frog.php');
    require ('Ape.php');

    $sheep = new Animal("Shaun");
    
    echo "<h2>Creating Object: Sheep </h2>";
    echo "Name: $sheep->name" ; // "shaun"
    echo "<br>";
    echo "Legs: $sheep->legs"; // 4
    echo "<br>";
    echo "Cold Blooded: $sheep->cold_blooded "; // "no"
    echo "<br>";
    echo "<br>";
    echo "<h2> Inheritances from Animal Class </h2>";
    // Membuat objek Frog -> panggil method nya
    $kodok = new Frog("Buduk");
    echo "<h3> Frog Class </h3>";
    echo ("Nama: ".$kodok->getName());
    echo "<br>";
    echo ("Legs: ".$kodok->getLegs());
    echo "<br>";
    echo ("Cold Blooded: ".$kodok->getColdBlooded());
    echo "<br>";
    // echo ("Jump: ");
    // echo ($kodok->jump());
    echo ("Jump: " .$kodok->jump());
    echo "<br>";

    // Membuat objek Ape
    $sungokong = new Ape("Kera Sakti");
    echo "<h3> Ape Class </h3>";
    echo ("Nama: ".$sungokong->getName());
    echo "<br>";
    echo ("Legs: ".$sungokong->getLegs());
    echo "<br>";
    echo ("Cold Blooded: ".$sungokong->getColdBlooded());
    echo "<br>";
    // Normal kalau pake cara ini:
    // echo ("Yell: ");
    // echo ($sungokong->yell());
    // echo "<br>";
    // //Ngebug kalo pake cara ini: -> pake return biar gak ngebug
    echo ("Yell: " .$sungokong->yell());

?>