<?php
    class Ape extends Animal {
        public $yellSound = "Auooo";

        public function __construct($name)
        {
            parent::__construct($name);
            $this->legs = 2;
        }
    
        public function yell()
        {
            return $this->yellSound;
        }
    }
?>