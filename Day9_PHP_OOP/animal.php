<?php 
    
    class Animal 
    {
        public $name;
        public $legs = 4;
        public $cold_blooded = "No";

        public function __construct($name)
        {
            $this->name = $name;
        }

        //Make set and get function from those variables ->
        
        // Set Functions :
        public function setName($name) 
        {
            $this->name = $name;
        }
        public function setColdBlooded($cold_blooded) {
            $this->cold_blooded = $cold_blooded;
        }
        public function setLegs($legs)
        {
            $this->legs = $legs;
        }

        // Get Functions:
        public function getLegs() 
        {
            return $this->legs;
        }
        public function getName () {
            return $this->name;
        }
        public function getColdBlooded() {
            return $this->cold_blooded;
        }
    }

?>