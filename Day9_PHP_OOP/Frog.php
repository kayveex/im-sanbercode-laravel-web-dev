<?php
    class Frog extends Animal
    {
        public $jumpSound = "Hop hop";
        public function jump()
        {
            return $this->jumpSound;
        }
    }
?>